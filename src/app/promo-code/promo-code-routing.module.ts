import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreatePromoCodeComponent} from './create-promo-code/create-promo-code.component'
import {ListPromoCodeComponent} from './list-promo-code/list-promo-code.component'

export const PromoCodeRoutes: Routes = [
  {
    path: 'create-promo-code',
    children: [{
      path: '',
      component: CreatePromoCodeComponent
  }
]},
  {
    path: '',
    children: [{
      path: '',
      component: ListPromoCodeComponent
  }
  ]}
];