import { Component, OnInit, ViewChild , AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';
import { GetListService } from '../../get-list.service';
import { PromoService } from '../promo.service';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import 'datatables.net';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { StorageService } from 'src/app/shared/services/storage.service';


export interface Lists {
  description: String;
  discount: Number;
  id: String;
  maximumDiscountValue: Number;
  maximumUsageCountPerUser: Number;
  minimumCartValue: Number;
  name: String;
  restaurantId: String;
  validityDate: String;
}
declare const $: any;

@Component({
  selector: 'app-list-promo-code',
  templateUrl: './list-promo-code.component.html',
  styleUrls: ['./list-promo-code.component.css']
})
export class ListPromoCodeComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  public pageLength;
  public dataSource = new MatTableDataSource<Lists>();
  restaurant = [];
  categoryList = [];
  selectedRestaurant = '';
  typeCategory = '';
  showLoader = true;
  restaurantId;
  page = 0;
  size = 6;
  focusConstitution$ = new Subject<string>();
  clickContitution$  = new Subject<string>();
  listForm: FormGroup;
  public restaurantName = '';
  constructor(
    private Service: PromoService,
    private formBuilder: FormBuilder,
    private Route: Router,
    private storage: StorageService,
    private List: GetListService) { }

  ngOnInit(): void {
    this.restaurantList();
    this.dataSource.paginator = this.paginator;
    this.listForm = this.formBuilder.group({
      Promo_code_Search: [null, Validators.required],
      restaurant_Id: [null, Validators.required],
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {

    });
  }

  formatterConstitution(result: any) {
    return result.name;
  }
  selectId(event) {
    console.log(event);
  }
  searchConstituon = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const inputFocus$ = this.focusConstitution$;
    return merge(debouncedText$, inputFocus$).pipe(
    map(term => (term.length < 0 ? []
    : this.restaurant.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

 restaurantList() {
  const query = {
    'searchText': ''
  };
  console.log('restaurant Name api query', query);
  this.List.restaurantNameList(query)
    .then(
      (res: any) => {
        console.log(res, 'restaurant Name api response');
        this.restaurant = res['data'];
        console.log(this.restaurant , 'restaurant list');
      },
      (err: Object) => {
        console.log(err, 'restaurant Name api error');
      }
    )
    .catch((err: Object) => {
      console.log(err, 'restaurant Name error catch');
    });

  }

  selectedRestaurantId(data) {
    this.restaurantId = data.item['id'];
    this.restaurantName = data.item['name'];
    console.log('restaurant selected', data);
    this.foodCategoryList(this.restaurantId);
  }

  createPromoCode() {
    this.Route.navigate(['/promo-code/create-promo-code']);
  }
  edit(value) {
    this.storage.setItem('editPromoCode', value);
    console.log(value, 'value');
    this.Route.navigate(['/promo-code/create-promo-code',  {edit: true}]);
  }
  searchCategory() {
    if (this.restaurantId) {
      this.foodCategoryList(this.restaurantId);
    }
  }
  onPaginate(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;

    // this.page = this.page + 1;
    this.foodCategoryList(this.restaurantId);
  }
  foodCategoryList(id) {
    const query = {
      'restaurantId': id,
      'searchText': this.typeCategory,
      'page': this.page,
      'size': this.size,
      'sortBy': 'name',
      'orderBy': 'ASC'
    };
    console.log('restaurant Name api query', query);
    this.Service.listPromoCode(query)
      .then(
        (res: any) => {
          console.log(res, 'restaurant Name api response');
          this.dataSource.data  = res['data'][0].promoCodeList as Lists[];
          this.pageLength = res['data'][0].totalPages * this.size;
          this.showLoader = false;
          console.log(this.dataSource.data , 'category List');
        },
        (err: Object) => {
          console.log(err, 'restaurant Name api error');
          this.showLoader = false;
        })
      .catch((err: Object) => {
        console.log(err, 'restaurant Name error catch');
    });
  }
}
