import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { GetListService } from '../../get-list.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';


import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserDetailsModalComponent } from '../../shared/user-details-modal/user-details-modal.component';

export interface List {
  blocked: Boolean;
  email: String;
  firstName: String;
  id: String;
  lastName: String;
  mobileNumber: String;
  photoUrl: String;
  restaurantName: String;
  userCountry: String;
  whatsAppNumber: String;
}
declare const $: any;
@Component({
  selector: 'app-cashier',
  templateUrl: './cashier.component.html',
  styleUrls: ['./cashier.component.css']
})
export class CashierComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  public displayedColumns = ['Photo', 'First Name', 'Last Name', 'Country', 'Restaurant' , 'Actions' , 'Block'];
  public dataSource = new MatTableDataSource<List>();
  page = 0;
  count = 0;
  public size = 5;
  // public page = 0;
  public pageLength;
  showLoader = true;
  constructor(
    private managerList: GetListService,
    private Route: Router,
    private modalService: NgbModal) {}

  ngOnInit() {
    this.getList(this.page, this.size);
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit() {
    setTimeout(() => {

    });
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
  onPaginate(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;
    console.log('pageNumber ', this.page + 1, ' pagesize ', this.size);
    this.page = this.page + 1;
    this.getList(this.page, this.size);
  }
  createCashier() {
    this.Route.navigate(['/create/user']);
  }

  getList(page, size) {

    const query = {
      'userRole': 'CASHIER',
      'searchText': '',
      'page': page,
      'size': size,
      'sortBy': 'firstName',
      'orderBy': 'ASC'
    };
    console.log('list api query', query);
    this.managerList.list(query)
      .then(
          (res: any) => {
            console.log(res, 'list api response');
            this.dataSource.data = res['data'][0].userList as List[];
            this.pageLength = res['data'][0].totalPages * size;
            console.log(this.pageLength, 'pagelength' );
              this.showLoader = false;
          },
          (err: Object) => {
              this.showLoader = false;
            console.log(err, 'list api error');
          }
      )
      .catch((err: Object) => {
        console.log(err, 'list error catch');
    });
  }
  showDetails(content) {
    // console.log(content);
    this.openModal(content);
  }
  openModal(content) {
    const modalRefrence = this.modalService.open(UserDetailsModalComponent, {
    backdrop: true,
    centered: true,
    keyboard: true,
    });
    modalRefrence.componentInstance.title = 'CASHIER DETAILS';
    modalRefrence.componentInstance.data = content;
    modalRefrence.componentInstance.icon = 'monetization_on';
  }
}
