import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminComponent} from './admin/admin.component';
import {CashierComponent} from './cashier/cashier.component';
import {DeliveryBoyComponent} from './delivery-boy/delivery-boy.component';
import {KitchenComponent} from './kitchen/kitchen.component';
import {ManagerComponent} from './manager/manager.component';
import { GlobalAdminComponent } from './global-admin/global-admin.component';
import { SupportComponent } from './support/support.component';
import { RouterModule } from '@angular/router';

import { UserRoutes } from './user-routing.module';
import { UserComponent } from './user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { MaterialModule } from '../app.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [AdminComponent, CashierComponent, DeliveryBoyComponent, KitchenComponent, ManagerComponent, GlobalAdminComponent, SupportComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    NgbModule
  ]
})
export class UserModule { }
