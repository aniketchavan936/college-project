import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer.component';
export const CustomerRoutes: Routes = [
  {
    path: '',
    children: [ {
      path: '',
      component: CustomerComponent
  }
]}
];
