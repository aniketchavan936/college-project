
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
export const createRoutes: Routes = [
  {
    path: '',
    children: [ {
      path: 'user',
      component: UserComponent
  }
]},
{
  path: '',
  children: [{
    path: 'restaurant',
    component: RestaurantComponent
}]
}
];

