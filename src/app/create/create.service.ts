import { Injectable } from '@angular/core';
import { URLConstants } from '../urlConstants';
import { HttpService } from '../shared/services/http.service';
@Injectable({
  providedIn: 'root'
})
export class CreateService {

  constructor(private http: HttpService) { }

  createUser(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.createUser, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  createRestaurant(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.createRestaurant, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
