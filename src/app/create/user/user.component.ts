// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, OnChanges, AfterViewInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormBuilder } from '@angular/forms';
import { CommonDataService } from '../../shared/services/common-data.service';
import { CreateService } from '../create.service';
import { MastersService } from '../../masters.service';
import { NotificationService } from '../../shared/services/notification.service';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { GetListService } from '../../get-list.service';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from '../../shared/services/storage.service';
import { isNgTemplate } from '@angular/compiler';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
declare const $: any;
interface FileReaderEventTarget extends EventTarget {
    result: string;
}

interface FileReaderEvent extends Event {
    target: EventTarget;
    getMessage(): string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  selectedValue = '+1';
  focusConstitution$ = new Subject<string>();
  clickContitution$  = new Subject<string>();
  restaurant = [];
  selectedRole;
  isValidRole = true;
  countryList = [];
  editDetails: Boolean = false;
  userdetails: any;
  countryCode;
  rolls;
  filepath;
  filename;
  task: AngularFireUploadTask;
  progressValue: Observable<number>;
  downloadableURL;
  idProofUrl;
  ifProoftask: AngularFireUploadTask;
  idproofProgress: Observable<number>;
  idProofPath;
  idProofName;
  addressUrl;
  addresstask: AngularFireUploadTask;
  addressProgress: Observable<number>;
  addressPath;
  addressName;
  otherUrl;
  othertask: AngularFireUploadTask;
  otherProgress: Observable<number>;
  otherPath;
  otherName;
  restaurantSelected = '';
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  editableData = {
    blocked: '',
    email: '',
    firstName: '',
    id: '',
    lastName: '',
    mobileNumber: '',
    photoUrl: '',
    restaurantName: '',
    userCountry: '',
    whatsAppNumber: '',
    userRole: '',
    addressUrl: '',
    idProofUrl: '',
    otherUrl: ''
  };
  matcher = new MyErrorStateMatcher();
  uuid: any;
  type: FormGroup;
  title = 'Create New User';
  constructor(private formBuilder: FormBuilder,
    private dataService: CommonDataService,
    private MasterService: MastersService,
    public notify: NotificationService,
    private List: GetListService,
    private Service: CreateService,
    private router: Router,
    private storage: StorageService,
    private activatedRoute: ActivatedRoute,
    private angularFire: AngularFireStorage) {
    this.countryCode = dataService.countryCodeData();
    this.rolls = dataService.userRoleData();
    // console.log(activatedRoute.snapshot.params.edit, 'routes');
    if (activatedRoute.snapshot.params.edit !== undefined) {
        this.editDetails =  activatedRoute.snapshot.params.edit;
        this.title = 'Edit User';
    } else {
        this.editDetails = false;
    }
    console.log('this.editDetails', this.editDetails);
    // console.log(this.countryCode, 'country Code');
    // console.log(this.storage.getItem('userDetails'));
    this.uuid = this.storage.getItem('userDetails');
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }
    ngOnInit() {
      this.restaurantList();
      this.getMasters();
      const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
      let numberDetails = [];
      let whatsAppNumber = [];
      if (this.editDetails) {
          this.userdetails = this.storage.getItem('editUserData');
          console.log('data in storage', this.userdetails);
          numberDetails = this.userdetails.mobileNumber.split('.');
          whatsAppNumber = this.userdetails.whatsAppNumber.split('.');
        //   console.log(numberDetails, 'number details');
          this.editableData = {
            blocked: this.userdetails.blocked,
            email: this.userdetails.email,
            firstName: this.userdetails.firstName,
            id: this.userdetails.id,
            lastName: this.userdetails.lastName,
            mobileNumber: numberDetails[1],
            photoUrl: this.userdetails.photoUrl,
            restaurantName: this.userdetails.restaurantName,
            userCountry: this.userdetails.userCountry,
            whatsAppNumber: whatsAppNumber[1],
            userRole: this.userdetails.userRole,
            addressUrl: this.userdetails.documentList[0].documentURL,
            idProofUrl: this.userdetails.documentList[1].documentURL,
            otherUrl: this.userdetails.documentList[2].documentURL
          };
        //   console.log(this.editableData, 'editabledata');
        // tslint:disable-next-line:triple-equals
        if ( this.selectedRole == 'ADMIN' ||  this.selectedRole == 'SUPPORT' ||  this.selectedRole == 'GLOBAL_ADMIN') {
            this.isValidRole = false;
        } else {
            this.isValidRole = true;
        }
      }

      this.type = this.formBuilder.group({
        firstName: [this.editableData.firstName, Validators.required],
        lastName: [this.editableData.lastName, Validators.required],
        email: [this.editableData.email, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
        mobile_code: [this.countryCode[0].code, Validators.required],
        mob_number: [this.editableData.mobileNumber, Validators.required],
        What_code: [this.countryCode[0].code, Validators.required],
        what_number: [this.editableData.whatsAppNumber, Validators.required],
        user_adddress: this.formBuilder.array([]),
        idProofDescription: [null],
        addProofDescription: [null],
        restaurant_Id: [null],
        otherDescription: [null]
       });
       if (this.editDetails) {
        this.type.patchValue({
            mobile_code: numberDetails[0],
            What_code: whatsAppNumber[0],
            idProofDescription: this.userdetails.documentList[1].description,
            addProofDescription: this.userdetails.documentList[0].description,
            otherDescription: this.userdetails.documentList[2].description
           });
            if (this.userdetails.userAddressList.length > 0) {
                const addresses = this.type.get('user_adddress') as FormArray;
                for (const item of this.userdetails.userAddressList) {
                    addresses.push(this.addresses(item));
                }
            }
        this.downloadableURL = this.editableData.photoUrl;
        this.addressUrl = this.editableData.addressUrl;
        this.idProofUrl = this.editableData.idProofUrl;
        this.otherUrl = this.editableData.otherUrl;
       }
        // Code for the Validator
        const $validator = $('.card-wizard form').validate({
            rules: {
                firstname: {
                    required: true,
                    minlength: 3
                },
                lastname: {
                    required: true,
                    minlength: 3
                },
                email: {
                    required: true,
                    minlength: 3,
                },
                mob_number: {
                    required: true,
                    minlength: 8,
                },
                what_number: {
                    required: true,
                    minlength: 8,
                },
                premise:  {
                    required: true,
                    minlength: 2
                },
                streetAddress:   {
                    required: true,
                    minlength: 2
                },
                locality:   {
                    required: true,
                    minlength: 2
                },
                administrativeArea:  {
                    required: true,
                    minlength: 2
                },
                landmark:  {
                    required: true,
                    minlength: 2
                },
                countryCode:   {
                    required: true,
                    minlength: 2
                },
            },

            highlight: function(element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
            },
            success: function(element) {
              $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
            },
            errorPlacement : function(error, element) {
              $(element).append(error);
            }
        });

        // Wizard Initialization
        $('.card-wizard').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-previous',

            onNext: function(tab, navigation, index) {
                var $valid = $('.card-wizard form').valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            },

            onInit: function(tab: any, navigation: any, index: any){

              // check number of tabs and fill the entire row
              let $total = navigation.find('li').length;
              let $wizard = navigation.closest('.card-wizard');

              let $first_li = navigation.find('li:first-child a').html();
              let $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
              $('.card-wizard .wizard-navigation').append($moving_div);

              $total = $wizard.find('.nav li').length;
             let  $li_width = 100/$total;

              let total_steps = $wizard.find('.nav li').length;
              let move_distance = $wizard.width() / total_steps;
              let index_temp = index;
              let vertical_level = 0;

              let mobile_device = $(document).width() < 600 && $total > 3;

              if(mobile_device){
                  move_distance = $wizard.width() / 2;
                  index_temp = index % 2;
                  $li_width = 50;
              }

              $wizard.find('.nav li').css('width',$li_width + '%');

              let step_width = move_distance;
              move_distance = move_distance * index_temp;

              let $current = index + 1;

              if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
                  move_distance -= 8;
              } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
                  move_distance += 8;
              }

              if (mobile_device) {
                  let x: any = index / 2;
                  vertical_level = parseInt(x);
                  vertical_level = vertical_level * 38;
              }

              $wizard.find('.moving-tab').css('width', step_width);
              $('.moving-tab').css({
                  'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
                  'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

              });
              $('.moving-tab').css('transition','transform 0s');
           },

            onTabClick : function(tab: any, navigation: any, index: any) {

                const $valid = $('.card-wizard form').valid();

                if (!$valid) {
                    return false;
                } else {
                    return true;
                }
            },

            onTabShow: function(tab: any, navigation: any, index: any) {
                let $total = navigation.find('li').length;
                let $current = index + 1;
                if (elemMainPanel) {
                  elemMainPanel.scrollTop = 0;
                }
                // console.log(elemMainPanel, 'element panel');
                // elemMainPanel.scrollTop = 0;
                const $wizard = navigation.closest('.card-wizard');

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $($wizard).find('.btn-next').hide();
                    $($wizard).find('.btn-finish').show();
                } else {
                    $($wizard).find('.btn-next').show();
                    $($wizard).find('.btn-finish').hide();
                }

                const button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                setTimeout(function(){
                    $('.moving-tab').text(button_text);
                }, 150);

                const checkbox = $('.footer-checkbox');

                if ( index !== 0 ) {
                    $(checkbox).css({
                        'opacity':'0',
                        'visibility':'hidden',
                        'position':'absolute'
                    });
                } else {
                    $(checkbox).css({
                        'opacity':'1',
                        'visibility':'visible'
                    });
                }
                $total = $wizard.find('.nav li').length;
               let  $li_width = 100/$total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if(mobile_device){
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width',$li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
                    move_distance -= 8;
                } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
                    move_distance += 8;
                }

                if(mobile_device){
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        });


        // Prepare the preview for profile picture
        $('#wizard-picture').change(function() {
            const input = $(this);

            if (input[0].files && input[0].files[0]) {
                const reader = new FileReader();

                reader.onload = function (e: any) {
                    $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                };
                reader.readAsDataURL(input[0].files[0]);
            }
        });

        $('[data-toggle="wizard-radio"]').click(function(){
            const wizard = $(this).closest('.card-wizard');
            wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
            $(this).addClass('active');
            $(wizard).find('[type="radio"]').removeAttr('checked');
            $(this).find('[type="radio"]').attr('checked', 'true');
        });

        $('[data-toggle="wizard-checkbox"]').click(function(){
            if ( $(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).find('[type="checkbox"]').removeAttr('checked');
            } else {
                $(this).addClass('active');
                $(this).find('[type="checkbox"]').attr('checked', 'true');
            }
        });

        $('.set-full-height').css('height', 'auto');

    }

    ngOnChanges(changes: SimpleChanges) {
        const input = $(this);

        if (input[0].files && input[0].files[0]) {
            const reader: any = new FileReader();

            reader.onload = function (e: any) {
                $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
            };
            reader.readAsDataURL(input[0].files[0]);
        }
    }
    ngAfterViewInit() {

        $( window ).resize( () => { $('.card-wizard').each(function(){
          setTimeout(() => {
            const $wizard = $(this);
            const index = $wizard.bootstrapWizard('currentIndex');
            let $total = $wizard.find('.nav li').length;
            let  $li_width = 100/$total;

            let total_steps = $wizard.find('.nav li').length;
            let move_distance = $wizard.width() / total_steps;
            let index_temp = index;
            let vertical_level = 0;

            let mobile_device = $(document).width() < 600 && $total > 3;
            if(mobile_device){
                move_distance = $wizard.width() / 2;
                index_temp = index % 2;
                $li_width = 50;
            }

            $wizard.find('.nav li').css('width',$li_width + '%');

            let step_width = move_distance;
            move_distance = move_distance * index_temp;

            let $current = index + 1;

            if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
                move_distance -= 8;
            } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
                move_distance += 8;
            }

            if(mobile_device){
                let x: any = index / 2;
                vertical_level = parseInt(x);
                vertical_level = vertical_level * 38;
            }

            $wizard.find('.moving-tab').css('width', step_width);
            $('.moving-tab').css({
                'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
                'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
            });

            $('.moving-tab').css({
                'transition': 'transform 0s'
            });
          }, 500);

        });
    });

  }
  selectRole(role) {
    //   console.log('peplacecode', role.replace(' ', '_'));
      this.selectedRole = role.replace(' ', '_');
      // tslint:disable-next-line:triple-equals
      if ( this.selectedRole == 'ADMIN' ||  this.selectedRole == 'SUPPORT' ||  this.selectedRole == 'GLOBAL_ADMIN') {
        this.isValidRole = false;
      } else {
        this.isValidRole = true;
      }
  }

  addresses(value?) {
    return this.formBuilder.group({
        premise: [value ? value.premise : null, Validators.required],
        countryName : [value ? value.countryName : 'India', Validators.required],
        streetAddress:  [value ? value.streetAddress : null, Validators.required],
        locality:  [value ? value.locality : null, Validators.required],
        subAdministrativeArea:  [value ? value.subAdministrativeArea : null, Validators.required],
        administrativeArea: [value ? value.administrativeArea : null, Validators.required],
        postalCode: [value ? value.postalCode : null, Validators.required],
        landmark:  [value ? value.landmark : null, Validators.required],
        countryCode:  [value ? value.countryCode : this.type.value.mobile_code, Validators.required],
        addressType: [value ? value.addressType : 'HOME', Validators.required],
    });
}

    // add more add
    AddMoreAdd() {
        this.getAddresses.push(this.addresses());
    }

    // remove add
    removeAdd(index) {
        this.getAddresses.removeAt(index);
    }

    get getAddresses() {
        return this.type.get('user_adddress') as FormArray;
    }

    createUsers() {
        // console.log(this.getAddresses.value, 'formdata');
        const query = {
            firstName: this.type.value.firstName,
            lastName: this.type.value.lastName,
            photoPath: `user/image/${this.filepath}`,
            photoUrl: `${this.downloadableURL}`,
            mobileNumber: this.type.value.mobile_code  + '.' + this.type.value.mob_number ,
            whatsAppNumber: this.type.value.What_code  + '.' + this.type.value.what_number,
            email: this.type.value.email,
            userRole: this.editDetails ? this.editableData.userRole : this.selectedRole,
            createdBy: this.uuid.id,
            restaurantId: this.type.value.restaurant_Id ? this.type.value.restaurant_Id.id : '',
            userAddressList: this.getAddresses.value,
            id: this.editableData.id,
            documentList: [
            {
                documentType: 'ID_PROOF',
                documentURL:  `${this.idProofUrl}`,
                documentPath: `user/Document/${this.idProofPath}`,
                description: this.type.value.idProofDescription
            },
            {
                documentType: 'ADDRESS_PROOF',
                documentURL:  `${this.addressUrl}`,
                documentPath: `user/Document/${this.addressPath}`,
                description: this.type.value.addProofDescription
            },
            {
                documentType: 'OTHER',
                documentURL: `${this.otherUrl}`,
                documentPath: `user/Document/${this.otherPath}`,
                description: this.type.value.otherDescription
            }
            ]
        };
        // console.log('create user api query', query);

        this.Service.createUser(query)
            .then(
                (res: any) => {
                    // console.log(res, 'create api response');
                    this.notify.showSuccess('User has been created successfully', 'Succesfully Created');
                    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                    this.router.onSameUrlNavigation = 'reload';
                    this.router.navigate(['create/user']);
                    // .toLowerCase()
                },
                (err: Object) => {
                    // console.log(err['error'].errorResponse.description, 'create api error');
                    this.notify.showError(err['error'].errorResponse.description,'Error !!!');
                    this.router.navigate([this.router.url]);
                }
            )
            .catch((err: Object) => {
                // console.log(err, 'create api error catch');
        });
    }

    async imageUpload(file) {
        const fileData = file.target.files[0];
        this.filename = file.target.files[0].name;
        // console.log('file', fileData);
        if (fileData) {
        this.filepath =  Math.random() + fileData;
        this.task = this.angularFire.upload(`user/image/${this.filepath}`, fileData);
        this.progressValue = this.task.percentageChanges();
        (await this.task).ref.getDownloadURL().then(url => {this.downloadableURL = url; });
        // console.log(this.filepath, 'filepath');
        } else {
        alert('No images selected');
        this.downloadableURL = ''; }
    }
    async idProofUpload(file) {
        const fileData = file.target.files[0];
        this.idProofName = file.target.files[0].name;
        // console.log('file', fileData);
        if (fileData) {
        this.idProofPath =  Math.random() + fileData;
        this.ifProoftask = this.angularFire.upload(`user/Document/${this.idProofPath}`, fileData);
        this.idproofProgress = this.ifProoftask.percentageChanges();
        (await this.ifProoftask).ref.getDownloadURL().then(url => {this.idProofUrl = url; });
        // console.log(this.idProofPath, 'filepath');
        } else {
        alert('No images selected');
        this.idProofUrl = ''; }
    }
    async addressUpload(file) {
        const fileData = file.target.files[0];
        this.addressName = file.target.files[0].name;
        // console.log('file', fileData);
        if (fileData) {
        this.addressPath =  Math.random() + fileData;
        this.addresstask = this.angularFire.upload(`user/Document/${this.addressPath}`, fileData);
        this.addressProgress = this.addresstask.percentageChanges();
        (await this.addresstask).ref.getDownloadURL().then(url => {this.addressUrl = url; });
        // console.log(this.addressPath, 'filepath');
        } else {
        alert('No images selected');
        this.addressUrl = ''; }
    }
    async otherUpload(file) {
        const fileData = file.target.files[0];
        this.otherName = file.target.files[0].name;
        // console.log('file', fileData);
        if (fileData) {
        this.otherPath =  Math.random() + fileData;
        this.othertask = this.angularFire.upload(`user/Document/${this.otherPath}`, fileData);
        this.otherProgress = this.othertask.percentageChanges();
        (await this.othertask).ref.getDownloadURL().then(url => {this.otherUrl = url; });
        // console.log(this.otherPath, 'filepath');
        } else {
        alert('No images selected');
        this.otherUrl = ''; }
    }
    getMasters() {
        const query = { };
        // console.log('country api query', query);
        this.MasterService.countryList(query)
            .then(
                (res: any) => {
                    this.countryList = res['data'];
                    // console.log(this.countryList, 'country api response');
                },
                (err: Object) => {
                    // console.log(err, 'country api error');
                }
            )
            .catch((err: Object) => {
                // console.log(err, 'country api error catch');
        });
    }

    restaurantList() {
        const query = {
          'searchText': ''
        };
        // console.log('restaurant Name api query', query);
        this.List.restaurantNameList(query)
          .then(
            (res: any) => {
            //   console.log(res, 'restaurant Name api response');
              this.restaurant = res['data'];
            //   console.log(this.restaurant , 'restaurant list');
              this.restaurant.map((item) => {
            //   console.log(item.name, 'resto !!!!!!!!!!!!!!!!!!!!!!!!!!', this.editableData.restaurantName);
                // tslint:disable-next-line:triple-equals
                if (item.name == this.editableData.restaurantName) {
                    // console.log(item.name, 'resto !!!!!!!!!!!!!!!!!!!!!!!!!!');
                    this.type.patchValue({
                        restaurant_Id: item
                    });
                    this.restaurantSelected = item.name;
                    // console.log(this.type.value.restaurant_Id, 'resto !!!!!!!!!!!!!!!!!!!!!!!!!!');
                }
              });
            },
            (err: Object) => {
            //   console.log(err, 'restaurant Name api error');
            }
          )
          .catch((err: Object) => {
            // console.log(err, 'restaurant Name error catch');
        });
    }
    formatterConstitution(result: any) {
        return result.name;
      }
      selectId(event) {
        // console.log(event);
      }
      searchConstituon = (text$: Observable<string>) => {
        const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
        const inputFocus$ = this.focusConstitution$;
        return merge(debouncedText$, inputFocus$).pipe(
        map(term => (term.length < 0 ? []
        : this.restaurant.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
        );
    }
}
