import { Injectable } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { URLConstants } from '../urlConstants';
@Injectable({
  providedIn: 'root'
})
export class FoodDishService {

  constructor(private http: HttpService) { }

  createFoodFish(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.createFoodDish, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  listFoodDish(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getFoodDishList, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  listFoodCategory(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getFoodCategoryList, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
