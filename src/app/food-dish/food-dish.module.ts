import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodRoutes } from './food-dish-routing.module';
import { CreateFoodDishComponent } from './create-food-dish/create-food-dish.component';
import { ListFoodDishComponent } from './list-food-dish/list-food-dish.component';
import { RouterModule } from '@angular/router';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { MaterialModule } from '../app.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [CreateFoodDishComponent, ListFoodDishComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(FoodRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    NgbModule,
    SharedModule
  ]
})
export class FoodDishModule { }
