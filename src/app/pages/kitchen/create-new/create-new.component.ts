import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-new',
  templateUrl: './create-new.component.html',
  styleUrls: ['./create-new.component.scss']
})
export class CreateNewComponent implements OnInit {

  createNewForm = new FormGroup({
    name: new FormControl(''),
    mobile:  new FormControl(''),
    whatsapp: new FormControl(''),
    username: new FormControl(''),
    email:  new FormControl(''),
    password: new FormControl(''),
    restaurant: new FormControl(''),
    status:  new FormControl(''),
    // password: new FormControl(''),
  });
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  saveImage(event) {
    const image = document.getElementById('output');
    console.log(image, 'imgg');
    console.log(event.target.files[0], 'image');
	  image['src'] = URL.createObjectURL(event.target.files[0]);
  }

  createNew() {

  }
  goBack() {
    this.router.navigate(['/kitchen']);
  }
}
