import { Injectable } from '@angular/core';
import { HttpService } from './../../shared/services/http.service';
import { URLConstants } from '../../urlConstants';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpService) { }

  login(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getlogin, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  getResendOtp(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.resendOtp, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }


  getSubmitOtp(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.submitOtp, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
