import { Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { PricingComponent } from './pricing/pricing.component';
import { LockComponent } from './lock/lock.component';
import { LoginComponent } from './login/login.component';

export const PagesRoutes: Routes = [

    {
        path: '',
        // component: LoginComponent,
        children: [ {
            path: '',
            component: LoginComponent
        }, {
            path: 'create/login',
            component: LoginComponent
        },
        //  {
        //     path: 'create/register',
        //     component: RegisterComponent
        // }
    ]
    }
];
