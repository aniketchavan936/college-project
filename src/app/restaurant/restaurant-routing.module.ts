import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantComponent } from './restaurant.component';
export const restaurantRoutes: Routes = [
  {
    path: '',
    children: [ {
      path: '',
      component: RestaurantComponent
  }
]}
];
