import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { restaurantRoutes } from './restaurant-routing.module';
import { RestaurantComponent } from './restaurant.component';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { MaterialModule } from '../app.module';
import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [RestaurantComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(restaurantRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    SharedModule,
    NgbModule
  ]
})
export class RestaurantModule { }
