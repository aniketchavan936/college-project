import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DitectivesModule } from './ditectives/ditectives.module';
import { LoaderComponent } from './loader/loader.component';
import { UserDetailsModalComponent } from './user-details-modal/user-details-modal.component';
import { MaterialModule } from '../app.module';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [LoaderComponent, UserDetailsModalComponent],
  imports: [
    CommonModule,
    DitectivesModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatTabsModule
    // MaterialModule,
  ],
  exports: [DitectivesModule, LoaderComponent]
})
export class SharedModule { }
