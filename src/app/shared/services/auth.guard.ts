import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public jwtToken;
  constructor(private storage: StorageService, private router: Router) {
    this.jwtToken = this.storage.getItem('jwtToken');
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (this.jwtToken) {
      return true;
    } else {
      console.log('guarded... !!!!!!!!!!');
      this.router.navigate(['/login']);
      return false;
    }
  }
}
