import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(public toastr: ToastrService) {}

  showSuccess(title, message) {
    Swal.fire({
      title: title,
      text: message,
      icon: 'success',
      confirmButtonText: 'Ok',
      width: '22rem',
    });
  }

  showError( title, message) {
    Swal.fire({
      title: title,
      text: message,
      icon: 'error',
      confirmButtonText: 'Ok',
      width: '22rem',
    });
  }

  showInfo(title, message) {
    Swal.fire({
      title: title,
      text: message,
      icon: 'info',
      confirmButtonText: 'Ok',
      width: '22rem',
    });
  }

  showWarning(title, message) {
    Swal.fire({
      title: title,
      text: message,
      icon: 'warning',
      confirmButtonText: 'Ok',
      width: '22rem',
    });
  }

  showQuestion(message, title) {
    Swal.fire({
      title: title,
      text: message,
      icon: 'question',
      confirmButtonText: 'Ok',
      width: '22rem',
    });
  }
}
