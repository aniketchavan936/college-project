import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpService } from './http.service';
import { CommonDataService } from './common-data.service';
import { NotificationService } from './notification.service';
import { StorageService } from './storage.service';
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [HttpService, CommonDataService, NotificationService, StorageService]
})
export class ServicesModule {  }
