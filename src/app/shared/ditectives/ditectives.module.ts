import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlyDigitsDirective } from './only-digit.directive';
import { OnlyAlphabetsDirective } from './only-alphabet.directive';
import { DecimalDirective } from './decimal.directive';
import { AlphanumaricDirective } from './alphanumaric.directive';



@NgModule({
  declarations: [OnlyDigitsDirective, OnlyAlphabetsDirective, DecimalDirective, AlphanumaricDirective],
  imports: [
    CommonModule
  ],
  exports: [OnlyDigitsDirective, OnlyAlphabetsDirective, DecimalDirective,AlphanumaricDirective]
})
export class DitectivesModule { }
