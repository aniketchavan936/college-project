import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuard } from './shared/services/auth.guard';

export const AppRoutes: Routes = [
    // {
    //   path: '',
    //   redirectTo: 'dashboard',
    //   pathMatch: 'full',
    // },
    // {
    //     path: '**',
    //     redirectTo: 'login'
    // },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [{
          path: '',
          loadChildren: './pages/pages.module#PagesModule'
        },
        {
            path: 'login',
            loadChildren: './pages/pages.module#PagesModule'
        }],
    }, {
      path: '',
      component: AdminLayoutComponent,
      canActivate: [AuthGuard],
      children: [
          {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    }, {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
    }, {
        path: 'forms',
        loadChildren: './forms/forms.module#Forms'
    }, {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
    }, {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
    }, {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule'
    }, {
        path: 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    }, 
    // {
    //     path: 'user',
    //     loadChildren: './userpage/user.module#UserModule'
    // },
     {
        path: 'dish',
        loadChildren: './timeline/timeline.module#TimelineModule'
    }, {
        path: 'pricing',
        loadChildren: './timeline/timeline.module#TimelineModule'
    }, {
        path: 'pages',
        loadChildren: './timeline/timeline.module#TimelineModule'
    },  {
        path: 'create',
        loadChildren: './create/create.module#CreateModule'
    },
    //  {
    //     path: 'admin',
    //     loadChildren: './admin/admin.module#AdminModule'
    // }, {
    //     path: 'manager',
    //     loadChildren: './manager/manager.module#ManagerModule'
    // }, {
    //     path: 'cashier',
    //     loadChildren: './cashier/cashier.module#CashierModule'
    // }, {
    //     path: 'kitchen',
    //     loadChildren: './kitchen/kitchen.module#KitchenModule'
    // },
     {
        path: 'customer',
        loadChildren: './customer/customer.module#CustomerModule'
    },
    //  {
    //     path: 'delivery-boy',
    //     loadChildren: './delivery-boy/delivery-boy.module#DeliveryBoyModule'
    // },
      {
        path: 'restaurant-list',
        loadChildren: './restaurant/restaurant.module#RestaurantModule'
    },  {
        path: 'food-category',
        loadChildren: './food-category/food-category.module#FoodCategoryModule'
    },  {
        path: 'food-dish',
        loadChildren: './food-dish/food-dish.module#FoodDishModule'
    }, {
        path: 'user',
        loadChildren: './user/user.module#UserModule'
    },  {
        path: 'promo-code',
        loadChildren: './promo-code/promo-code.module#PromoCodeModule'
    }
  ]}
];
