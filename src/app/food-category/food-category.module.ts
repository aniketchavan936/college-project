import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FoodRoutes } from './food-category-routing.module';
import { CreateFoodCategoryComponent } from './create-food-category/create-food-category.component';
import { ListFoodCategoryComponent } from './list-food-category/list-food-category.component';
import { RouterModule } from '@angular/router';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { MaterialModule } from '../app.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [CreateFoodCategoryComponent, ListFoodCategoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(FoodRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    NgbModule
  ]
})
export class FoodCategoryModule { }
