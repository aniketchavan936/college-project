import { Injectable } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { URLConstants } from '../urlConstants';
@Injectable({
  providedIn: 'root'
})
export class FoodCategoryService {

  constructor(private http: HttpService) { }

  createFoodCategory(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.createFoodCategory, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  listFoodCategory(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getFoodCategoryList, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

}
