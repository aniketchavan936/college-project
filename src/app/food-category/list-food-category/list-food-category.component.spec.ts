import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFoodCategoryComponent } from './list-food-category.component';

describe('ListFoodCategoryComponent', () => {
  let component: ListFoodCategoryComponent;
  let fixture: ComponentFixture<ListFoodCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListFoodCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFoodCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
