import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GetListService } from '../../get-list.service';
import { FoodCategoryService } from '../food-category.service';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import 'datatables.net';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { StorageService } from '../../shared/services/storage.service';


export interface Lists {
  description: String;
  id: String;
  imagePath: String;
  imageURL: String;
  name: String;
  restaurantId: String;
}
declare const $: any;
@Component({
  selector: 'app-list-food-category',
  templateUrl: './list-food-category.component.html',
  styleUrls: ['./list-food-category.component.css']
})


export class ListFoodCategoryComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  public pageLength;
  public dataSource = new MatTableDataSource<Lists>();
  restaurant = [];
  categoryList = [];
  selectedRestaurant = '';
  typeCategory = '';
  showLoader = true;
  restaurantId;
  page = 0;
  size = 8;
  modalDetails: any;
  focusConstitution$ = new Subject<string>();
  clickContitution$  = new Subject<string>();
  listForm: FormGroup;
  constructor(
    private Service: FoodCategoryService,
    private formBuilder: FormBuilder,
    private Route: Router,
    private storage: StorageService,
    private modalService: NgbModal,
    private List: GetListService) { }

  ngOnInit(): void {
    this.restaurantList();
    this.dataSource.paginator = this.paginator;
    this.listForm = this.formBuilder.group({
      food_category_Search: [null, Validators.required],
      restaurant_Id: [null, Validators.required],
     });
  }

  ngAfterViewInit() {
    setTimeout(() => {

    });
  }
  edit(value) {
    this.storage.setItem('editFoodCategory', value);
    console.log(value, 'value');
    this.Route.navigate(['/food-category/create-category',  {edit: true}]);
  }

  formatterConstitution(result: any) {
    return result.name;
  }
  selectId(event) {
    console.log(event);
  }
  searchConstituon = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const inputFocus$ = this.focusConstitution$;
    return merge(debouncedText$, inputFocus$).pipe(
    map(term => (term.length < 0 ? []
    : this.restaurant.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

 restaurantList() {
  const query = {
    'searchText': ''
  };
  console.log('restaurant Name api query', query);
  this.List.restaurantNameList(query)
    .then(
      (res: any) => {
        console.log(res, 'restaurant Name api response');
        this.restaurant = res['data'];
        console.log(this.restaurant , 'restaurant list');
      },
      (err: Object) => {
        console.log(err, 'restaurant Name api error');
      }
    )
    .catch((err: Object) => {
      console.log(err, 'restaurant Name error catch');
  });

  }

  selectedRestaurantId(data) {
    this.restaurantId = data.item['id'];
    console.log('restaurant selected', data);
    this.foodCategoryList(this.restaurantId);
  }

  createCategory() {
    this.Route.navigate(['/food-category/create-category']);
  }
  searchCategory() {
    if (this.restaurantId) {
      this.foodCategoryList(this.restaurantId);
    }
  }

  onPaginate(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;

    // this.page = this.page + 1;
    this.foodCategoryList(this.restaurantId);
  }

  foodCategoryList(id) {
    const query = {
      'restaurantId': id,
      'searchText': this.typeCategory,
      'page': this.page,
      'size': this.size,
      'sortBy': 'name',
      'orderBy': 'ASC'
    };
    console.log('restaurant Name api query', query);
    this.Service.listFoodCategory(query)
      .then(
        (res: any) => {
          console.log(res, 'restaurant Name api response');
          this.dataSource.data  = res['data'][0].categoryList as Lists[];
          this.pageLength = res['data'][0].totalPages * this.size;
          this.showLoader = false;
          console.log(this.dataSource.data , 'category List');
        },
        (err: Object) => {
          console.log(err, 'restaurant Name api error');
          this.showLoader = false;
        }
      )
      .catch((err: Object) => {
        console.log(err, 'restaurant Name error catch');
    });
  }

  foodDetails(content, values) {
    this.modalDetails = values;
    console.log('values', this.modalDetails);
    this.openModal(content);
  }

  openModal(content) {
    this.modalService.open(content, {
    backdrop: true,
    centered: true,
    keyboard: true,
    });
  }
}
