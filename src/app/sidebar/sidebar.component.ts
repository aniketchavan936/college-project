import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

// Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

// Menu Items
export const ROUTES: RouteInfo[] = [{
        path: '/dashboard',
        title: 'Overview',
        type: 'link',
        icontype: 'dashboard'
    },{
        path: '/restaurant-list',
        title: 'Restaurant',
        type: 'link',
        icontype: 'restaurant',
        // collapse: 'components',
        // children: [
        //     {path: 'buttons', title: 'Buttons', ab:'B'},
        //     {path: 'grid', title: 'Grid System', ab:'GS'},
        //     {path: 'panels', title: 'Panels', ab:'P'},
        //     {path: 'sweet-alert', title: 'Sweet Alert', ab:'SA'},
        //     {path: 'notifications', title: 'Notifications', ab:'N'},
        //     {path: 'icons', title: 'Icons', ab:'I'},
        //     {path: 'typography', title: 'Typography', ab:'T'}
        // ]
    },
    {
        path: '/food-category',
        title: 'Food Categories',
        type: 'link',
        icontype: 'food_bank'
    },
    {
        path: '/food-dish',
        title: 'Food Dishes',
        type: 'link',
        icontype: 'fastfood'
    },
    {
        path: '/promo-code',
        title: 'Promo Codes',
        type: 'link',
        icontype: 'present_to_all'
    },
    {
        path: '/pricing',
        title: 'Online Order',
        type: 'link',
        icontype: 'mobile_screen_share'
    },
    {
        path: '/user',
        title: 'User',
        type: 'sub',
        icontype: 'person',
        collapse: 'user',
        children: [
            // {path: 'restaurant', title: 'Restaurant', ab: 'R'},
            // {path: 'user', title: 'User', ab: 'U'},
            {path: 'manager', title: 'Manager', ab: 'account_circle'},
            {path: 'cashier', title: 'Cashier', ab: 'monetization_on'},
            {path: 'kitchen', title: 'Kitchen', ab: 'kitchen'},
            {path: 'delivery-boy', title: 'Delivery-boy', ab: 'moped'},
            {path: 'admin', title: 'Admin', ab: 'perm_identity'},
            {path: 'global-admin', title: 'Global Admin', ab: 'people_alt'},
            {path: 'support', title: 'Support', ab: 'support_agent'}
        ]
    },
    
    // {
    //     path: '/forms',
    //     title: 'Forms',
    //     type: 'sub',
    //     icontype: 'content_paste',
    //     collapse: 'forms',
    //     children: [
    //         {path: 'regular', title: 'Regular Forms', ab:'RF'},
    //         {path: 'extended', title: 'Extended Forms', ab:'EF'},
    //         {path: 'validation', title: 'Validation Forms', ab:'VF'},
    //         {path: 'wizard', title: 'Wizard', ab:'W'}
    //     ]
    // },
    // {
    //     path: '/manager',
    //     title: 'Manager',
    //     type: 'link',
    //     icontype: 'account_circle',
    // },
    // {
    //     path: '/cashier',
    //     title: 'Cashier',
    //     type: 'link',
    //     icontype: 'monetization_on',
    // },
    // {
    //     path: '/delivery-boy',
    //     title: 'Delivery Boy',
    //     type: 'link',
    //     icontype: 'moped'

    // },
    // {
    //     path: '/kitchen',
    //     title: 'Kitchen',
    //     type: 'link',
    //     icontype: 'kitchen'

    // },
    // {
    //     path: '/customer',
    //     title: 'Customers',
    //     type: 'link',
    //     icontype: 'local_pizza'
    // },
    // {
    //     path: '/admin',
    //     title: 'Admin',
    //     type: 'link',
    //     icontype: 'perm_identity'
    // },
    // {
    //     path: '/pages',
    //     title: 'Pages',
    //     type: 'sub',
    //     icontype: 'image',
    //     collapse: 'pages',
    //     children: [
    //         {path: 'pricing', title: 'Pricing', ab: 'P'},
    //         {path: 'timeline', title: 'Timeline Page', ab: 'TP'},
    //         {path: 'login', title: 'Login Page', ab: 'LP'},
    //         {path: 'register', title: 'Register Page', ab: 'RP'},
    //         {path: 'lock', title: 'Lock Screen Page', ab: 'LSP'},
    //         {path: 'user', title: 'User Page', ab: 'UP'}
    //     ]
    // }
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    ps: any;
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
